<?php
require_once ('app/boot.php');
include 'template/header.php';

$contact_form = new \helpers\forms('contact');

// build field list
$contact_form->fields = array(

	'first_name' => array(
		'required' => true,
	),

	'surname' => array(
		'required' => true,
	),

	'email' => array(
		'type' => 'email',
		'label' => 'Email Address',
		'required' => true,
	),

	'gender' =>  array(
		'type' => 'select',
		'options' => array (
			'female' => 'Female',
			'male' => 'Male',
		),
		'required' => true,
	),

	'age' => array(
		'type' => 'number',
		'label' => 'Age',
		'min' => 0,
		'max' => 120,
		'required' => true,
	),

	'message' => array(
		'type' => 'textarea',
	),

	'newsletter' => array(
		'type' => 'checkbox',
		'options' => array(
			'yes' => 'Do you want to receive our newsletter?'
		),
	),

	'service' => array(
		'type' => 'radio',
		'options' => array(
			'development' => 'Development',
			'design' => 'Design',
			'optimisation' => 'Optimisation',
			'Paid Search' => 'Paid Search',
		),
		'required' => true
	),

	'recaptcha' => array(
		'type' => 'recaptcha',
		'required' => true,

	)
);

// process form if submitted
if (!empty($_POST)) {

	$db = new \data\database;

	$validate = new \helpers\validate();
	$contact_form = $validate->validateAll($contact_form, $_POST);

    // if submission is validated, enter in to DB
	if ($contact_form->validated == true) {
		$values = $_POST;
		unset($values['recaptcha'], $values['token'], $values['Submit']);

		$form_sent = $db->insert('submissions', $values);

		if ($form_sent) {
			addAlert('Form submitted', 'success');
		} else {
			addAlert('Form error - please try again later', 'danger');
		}

	} else {
		addAlert('Form error - please see highlighted fields', 'danger');
	}
}
?>

<h1>Contact</h1>

<div class="row">
	<div class="js-refresh col-12 col-xl-6">

        <?= showAlert(); ?>

<?php
if (!isset($form_sent)) {

    echo $contact_form->formStart('post', null, 'needs-validation was-validated');

    foreach ($contact_form->fields as $name => $options) {

        /* In this case we can use the name as the input ID and label value. They all have the same CSS class */
        if (!isset($options['label'])) {
            $options['label'] = ucwords(str_replace('_', ' ', $name));
        }
        $options['class'] = 'form-control';
        $options['id'] = $name;

        echo $contact_form->input($name, $options);
    }

    echo $contact_form->formEnd(true);
}
?>

	</div>
</div>