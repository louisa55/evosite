<!DOCTYPE html>
	<html lang=en>
		<head>

			<!-- Complete Bootstrap CSS -->
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<script src="resources/main.js"></script>
		</head>

		<body>

			<nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<a class="navbar-brand" href="#">Louisa Watson</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarCollapse">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item<?= $_SERVER['PHP_SELF'] == '/index.php' ? ' active' : ''; ?>">
                            <a href="index.php" class="nav-link">Forms</a>
                        </li>
                        <li class="nav-item<?= $_SERVER['PHP_SELF'] == '/navigation.php' ? ' active' : ''; ?>">
                            <a href="navigation.php" class="nav-link">Nested Navigation</a>
                        </li>
					</ul>
				</div>
			</nav>

			<div class="container">