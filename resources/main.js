$(function() {

    $('form').submit(function( event ) {

        event.preventDefault();

        $url = $(this).attr('action');
        $data = $(this).serialize();

        $.ajax({
            url: $url,
            type: "POST",
            data: $data,
            success: function (response) {
                $('.js-refresh').each(function (index) {
                    $(this).html($(response).find('.js-refresh').eq(index).html());
                });
            }
        });

    });

});