<?php
require_once ('app/boot.php');
include 'template/header.php';

$db = new \data\database;
$navigation = $db->selectAll('test');

// function to generate list according to matching parent ID
function buildNavigationList($navigation, $parent_id) {

	if (isset($navigation[$parent_id])) {

		echo '<ul>';

		foreach ($navigation[$parent_id] as $nav) {

			echo '<li>' . $nav['title'];
			buildNavigationList($navigation, $nav['test_id']);
			echo '</li>';
		}

		echo '</ul>';
	}
}

// group navigation according to parent
foreach ($navigation as $nav) {
	$nested_navigation[$nav['parent_test_id']][$nav['test_id']] = $nav;
}

// start navigation list using parent ID 0 / top level items
buildNavigationList($nested_navigation, 0);