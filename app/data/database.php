<?php

namespace data {

/**
 * Database connection and very basic queries.
 */

	use PDO;

	class database
	{

		private $host = 'localhost';
		private $username = 'root';
		//TODO
		private $password = 'change_this';
		private $dbname = 'form_lib';

		private $dbh;
		private $stmt;

		public function __construct()
		{
			// Set DSN
			$dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;

			// Create a new PDO instance
			try {
				$this->dbh = new \PDO($dsn, $this->username, $this->password,
					array(PDO::ATTR_PERSISTENT => true, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
			} // Catch any errors
			catch (PDOException $e) {
				echo $this->error = $e->getMessage();
			}

			$this->dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
		}

		public function __destruct()
		{
			$this->dbh = null;
		}

		public function bind($param, $value = null, $type = null)
		{

			$value = $this->clean($value);

			if (is_null($type)) {
				switch (true) {
					case is_int($value):
						$type = PDO::PARAM_INT;
						break;
					case is_bool($value):
						$type = PDO::PARAM_BOOL;
						break;
					case is_null($value):
						$type = PDO::PARAM_NULL;
						break;
					default:
						$type = PDO::PARAM_STR;
				}
			}

			$this->stmt->bindValue($param, $value, $type);
		}

		function clean($var)
		{
			if (is_array($var)) {
				return filter_var_array($var, FILTER_SANITIZE_STRING);
			} else {
				return filter_var($var, FILTER_SANITIZE_STRING);
			}
		}

		public function execute()
		{
			$success = $this->stmt->execute();
			return $success;
		}

		function insert($table, $values)
		{
			$sql = "INSERT INTO " . $table . " (";
			$sql .= '`' . implode('`,`', array_keys($values)) . '`';
			$sql .= ') VALUES (:' . implode(', :', array_keys($values)) . ')';

			$this->query($sql);
			foreach ($values as $key => $value) {
				$this->bind($key, $value);
			}
			return $this->execute();

		}

		public function query($query)
		{
			$this->stmt = $this->dbh->prepare($query);
		}

		public function selectAll($table)
		{
			$sql = "SELECT * FROM " . $table;

			$query = $this->query($sql);
			$this->execute();
			return $this->stmt->fetchAll(PDO::FETCH_ASSOC);

		}
	}
}
?>