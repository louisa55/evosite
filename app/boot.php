<?php

session_start();
spl_autoload_register('autoload');


function autoload($className) {

	$namespace = str_replace("\\","/",__NAMESPACE__);
	$className = str_replace("\\","/",$className);

	require_once 'app/' . (empty($namespace) ? '' : $namespace . '/') . '/' . $className . '.php';
}

// add alert to be shown
function addAlert($message, $type) {
	$_SESSION['alerts'][] = array('type' => $type, 'message' => $message);
}

// displays all alerts and clears queue of ones to be displayed
function showAlert() {

	if (isset($_SESSION['alerts'])) {
		foreach ($_SESSION['alerts'] as $alert) {
			echo '<div class="alert alert-' . $alert['type'] . '">' . $alert['message'] . '</div>';
		}

		unset($_SESSION['alerts']);
	}
}
?>