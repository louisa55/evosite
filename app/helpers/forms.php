<?php

namespace helpers;

/**
 * HTML form and input builder
 *
 * @author Louisa Watson
 */

class forms {

	var $formId;

	function __construct($formId) {

		$this->formId = $formId;
		$this->error = false;
		$this->validated = false;

	}

	/**
	 * Display an error message
	 *
	 * @param array $options
	 *
	 * @return string
	 */
	public function error($options) {

		$output = '';

		if (isset($options['error'])) {
			$output = '<div class="invalid-feedback">' . $options['error'] . '</div>';
		}

		return $output;
	}

	/**
	 * Select the correct input field and build input defaults
	 *
	 * @param string $name
	 * @param array $options
	 *
	 * @return string
	 */
	public function input($name, $options) {

		$defaults = array(

			// autocomplete attribute
			'autocomplete' => false,

			// autocomplete attribute
			'disabled' => false,

			// style class to apply to input
			'class' => '',

			// input ID
			'id' => 'form_' . $this->formId . '_' . strtolower($name),

			// label text
			'label' => '',

			// label style class
			'label_class' => '',

			// min attribute
			'min' => false,

			// max attribute
			'max' => false,

			// placeholder
			'placeholder' => false,

			// readonly attribute
			'readonly' => false,

			// required attribute
			'required' => false,

			// the type of input field - default is text
			'type' => 'text',

			// value for the input
			'value' => '',
		);

		$options = array_merge($defaults, $options);

		switch ($options['type']) {
			case 'checkbox':
			case 'radio':
				$output = $this->radioCheckbox($name, $options);
				break;
			case 'hidden':
				$output = '<input' . $this->generateSettings($name, $options, $options['type']);

				if (isset($options['value'])) {
					$output .= ' value = "' . $options['value'] . '"';
				}

				$output .= '/>' . $options['value'];
				break;
			case 'recaptcha':
				$output = $this->recaptcha($options);
				break;
			case 'select':
				$output = $this->select($name, $options);
				break;
			case 'submit':
				$output = $this->submit($name, $options);
				break;
			case 'textarea':
				$output = $this->textarea($name, $options);
				break;
			case 'email':
			case 'number':
			case 'password':
			case 'text':
				$output = $this->label($options);
				$output .= '<input' . $this->generateSettings($name, $options, $options['type']);

				if (isset($options['value'])) {
					$output .= ' value = "' . $options['value'] . '"';
				}

				$output .= '/>';
				break;
		}

		if (isset($output)) {

			$output .= $this->error($options);
			return $output;

		}
	}

	public function formStart($method, $action = null, $class) {

		$token = md5(uniqid(microtime(), true));
		$_SESSION['forms'][$this->formId]['token'] = $token;
		$options =  array(
			'type' => 'hidden',
			'value' => $token,
		);

		$this->fields['token'] = $options;

		$output = '<form method="' . $method . '" id ="' . $this->formId . '"' . ($class != null ? ' class="' . $class . '"' : '') . '>';

		return $output;

	}

	public function formEnd($submitButton = false) {

		$output = '';

		if ($submitButton == true) {
			$output .= $this->input('Submit', array(
				'class' => 'btn btn-lg btn-primary mt-3',
				'type' => 'submit',
			));
		}

		$output .= '</form>';
		return $output;

	}

	private function generateSettings($name, $options) {

		$output = ' id="' . $options['id'] . '"';
		$output .= ' name="' . $name . '"';
		$output .= ' type="' . $options['type'] . '"';

		if ($options['autocomplete']) {
			$output .= ' autocomplete="off"';
		}

		if ($options['class']) {
			$output .= ' class="' . $options['class'] . '"';
		}

		if ($options['disabled']) {
			$output .= ' disabled="disabled"';
		}

		if ($options['min']) {
			$output .= ' min="' . $options['min'] . '"';
		}

		if ($options['max']) {
			$output .= ' max="' . $options['max'] . '"';
		}

		if ($options['placeholder']) {
			$output .= ' placeholder="' . $options['placeholder'] . '"';
		}

		if ($options['readonly']) {
			$output .= ' readonly="readonly"';
		}

		if ($options['required']) {
			$output .= ' required';
		}

		return $output;
	}

	public function label($options) {

		$output = '';

		if (isset($options['label'])) {
			$output = '<label';

			$output .= ' for="' . $options['id'] . '"';
			$output .= ' class="mt-2 ' . $options['label_class'] . '"';
			$output .= '>' . $options['label'] . '</label>';
		}

		return $output;
	}

	public function recaptcha($options) {

		$number_1 = rand(0, 9);
		$number_2 = rand(0, 9);

		$options = array(
			'type' => 'number',
			'required' => 'true',
			'label' => 'Prove you are not a robot: ' . $number_1 . ' + ' . $number_2 . ' = ',
			'class' => isset($options['class']) ? $options['class'] : '',
		);

		$_SESSION['forms'][$this->formId]['recaptcha'] = $number_1 + $number_2;
		$output = $this->input('recaptcha', $options);

		return $output;
	}

	public function radioCheckbox($name, $options) {

		$output = '';
		foreach ($options['options'] as $option => $label) {

			$options['id'] = $this->formId . '_' . $option;
			$output .= '<label for="' . $options['id'] . '">' . $label . '</label><br>';
			$output .= '<input value="' . $option . '" ' . $this->generateSettings($name, $options);
			if (isset($options['value']) && $option == $options['value']) {
				$output .= ' checked';
			}
			$output .= '>';
		}
		return $output;
	}

	public function submit($name, $options) {

		$output = '<button ' . $this->generateSettings($name, $options, 'submit') . '>' . $name . '</button>';

		return $output;
	}

	public function select($name, $options) {

		$output = $this->label($options);
		$output .= '<select ' . $this->generateSettings($name, $options, 'select') . '>';

		foreach ($options['options'] as $option => $label) {
			$output .= '<option value="' . $option . '"';
			if (isset($options['value']) && $option == $options['value']) {
				$output .= ' selected';
			}
			$output .= '>' . $label . '</option>';
		}

		$output .= '</select>';

		return $output;
	}

	public function textarea($name, $options) {

		$output = $this->label($options);
		$output .= '<textarea' . $this->generateSettings($name, $options) .'>';

		if (isset($options['value'])) {
			$output .= $options['value'];
		}

		$output .= '</textarea>';

		return $output;
	}
}