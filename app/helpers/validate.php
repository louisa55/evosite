<?php

namespace helpers;

/**
 * Validates form submissions based on their presets.
 */
class validate
{

	/**
	 * Checks a form's fields validate including it's token
	 *
	 * @param object $form
	 *
	 * @return object
	 */
	public function validateAll($form, $values) {

		if (!isset($_SESSION['forms'][$form->formId]['token'])
			|| !isset($values['token'])
			|| $values['token'] != $_SESSION['forms'][$form->formId]['token']) {
			$form->error = '1';
		}

		foreach($form->fields as $name => &$field) {

			if (!isset($values[$name])) {
				$values[$name] = '';
			}

			$this->validate($form, $name, $values[$name]);

		}

		if ($form->error != true) {
			$form->validated = true;
		}

		return $form;
	}


	/**
	 * Check input validation based on rules set for field
	 *
	 * @param object $form
	 * @param string $name
	 * @param string $value
	 *
	 * @return object
	 */
	public function validate($form, $name, $value) {

		$field = &$form->fields[$name];
		$field['value'] = $value;

		if (isset($field['required'])) {

			if (trim($value) == '') {
				$form->error = true;
				$field['error'] = 'Field required';
			}
		}

		if (isset($field['type']) && $field['type'] == 'email') {

			if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
				$form->error = true;
				$field['error'] = 'Email invalid';
			}

		} else if (isset($field['type']) && $field['type'] == 'recaptcha') {

			if (!isset($_SESSION['forms'][$form->formId]['recaptcha'])
				|| $field['value'] != $_SESSION['forms'][$form->formId]['recaptcha']) {
				$form->error = true;
				$field['error'] = 'Incorrect recaptcha!';
			}

			unset($_SESSION['forms'][$form->formId]['recaptcha']);
		}

		return $form;

	}

}